﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BruktBok.Models
{
    public class PaymentSucess
    {
        public PaymentSucess() { }
        public int Id { get; set; }
        public string PaymentId { get; set; }

 


        public DateTime Date { get; set; }
        
        [Required, DataType(DataType.EmailAddress)]
        public string BuyerEmail { get; set; }

 

        public string OwnerEmail { get; set; }
        public string OderNumber { get; set; }
    }
}