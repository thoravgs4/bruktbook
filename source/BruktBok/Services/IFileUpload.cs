﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace BruktBok.Services
{
/// <summary>
/// Upload file interface.
/// </summary>
    public interface IFileUpload
    {
        void uploadImage(IFormFile formFile) { }
    }
}