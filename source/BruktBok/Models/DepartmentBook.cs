﻿namespace BruktBok.Models
{
    public class DepartmentBook
    {
        public DepartmentBook() {}
        
        public int DepartmentId { get; set; }
        public Department Department { get; set; }
        
        public int BookId { get; set; }
        public Book Book { get; set; }
    }
    
}