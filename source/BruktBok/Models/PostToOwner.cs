﻿using System;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace BruktBok.Models
{
    public  static class PostToOwner
    {
        public static async Task SendToOwner(ContactUs contact)
        {
            var apiKey = Environment.GetEnvironmentVariable("sendgrid_api");
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("mohammedcm@live.com", "Admin");
            var subject = "Mail from ";
            var to = new EmailAddress(contact.Email, contact.Message);
            // var content= new Content(kontakt.PhoneNumber.ToString());
            
            var msg = MailHelper.CreateSingleEmail(from, to, subject,"", contact.Message);
            var response = await client.SendEmailAsync(msg);
        }
    }
}