﻿using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;
using BruktBok.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Hosting;

namespace BruktBok.Services
{
    /// <summary>
    ///     this class implements the IFileUpload
    /// to upload files (image files in this case)
    /// and save in image folder. 
    /// </summary>
    public class FileUpload : IFileUpload
    {
        private IHostEnvironment _env;

        public FileUpload(IHostEnvironment env)
        {
            _env = env;
        }
        
        public async void uploadImage(IFormFile file)
        {
            if (file != null )
            {
                var filename = file.FileName;
                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images", filename);
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }

               
            }
            
        }
    }

   
}