﻿namespace BruktBok
{
    public class StripeSettings
    {
        public string SecretKey { get; set; }
        
        public string PublishableKey { get; set; }
    }
}