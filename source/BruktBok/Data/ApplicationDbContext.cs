﻿using System;
using System.Collections.Generic;
using System.Text;
using BruktBok.Models;
using BruktBok.Services;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BruktBok.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public  DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Faculty> Faculties { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<DepartmentBook> DepartmentBooks { get; set; }

        public DbSet<ContactSeller> ContactSellers { get; set; }

        public DbSet<Payment> Payments { get; set; }
        public DbSet<PaymentSucess> PaymentSucesses { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<DepartmentBook>()
                .HasKey(db => new {db.BookId , db.DepartmentId });
        }
    }
}
