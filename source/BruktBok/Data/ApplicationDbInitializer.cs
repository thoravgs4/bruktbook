﻿using System;
using System.Collections.Generic;
using System.Linq;
using BruktBok.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BruktBok.Data
{
    public class ApplicationDbInitializer
    {
        
        public static void Initialize(ApplicationDbContext db, bool dev, UserManager<ApplicationUser> um)
        {
            var engineering = new Faculty{Name = "Engineering"};
            var health = new Faculty{Name = "Health"};
            var economics= new Faculty{Name = "economy"};
            var other = new Faculty("other");
            var d1 = new Department("Data");
            var d2 = new Department("Elektro");
            var d3 = new Department("Bygg");
            var d4 = new Department("Mekatronikk");
            var nurse  = new Department("Sykepleie");
            var varnepleie = new Department("Varnepleie");
            var admin = new ApplicationUser {UserName = "filmon", Email = "filmom18@uia.no", NickName = "fil"};
            var firstPayment = new Payment();

            // Run migrations if we're not in development mode
            if (!dev)
            {
                
                db.Database.Migrate(); //

                if (db.Faculties.Count() != 0)
                {
                    return;
                }
                
                um.CreateAsync(admin, "Password1.");
                
                db.Faculties.Add(engineering);
                db.Faculties.Add(health);
                db.Faculties.Add(economics);
                db.Faculties.Add(other);

                // initialise  Departments here.
                d1.Faculty = engineering;
                d2.Faculty = engineering;
                d3.Faculty = engineering;
                d4.Faculty = engineering;
           
                nurse.Faculty = health;
                varnepleie.Faculty = health;
                db.Departments.AddRange(d1,d2,d3,d4,nurse,varnepleie);

                //adding departments to Engeenering Fuculty
                engineering.Departments.Add(d1);
                engineering.Departments.Add(d2);
                engineering.Departments.Add(d3);
                engineering.Departments.Add(d4);
           
                // adding health departments 
                health.Departments.Add(nurse);
                health.Departments.Add(varnepleie);
               
                db.SaveChanges();
                
                return;
            }
      
            

            db.Database.EnsureDeleted();


            db.Database.EnsureCreated();
            
            // initialise Faculties here
            
            db.Faculties.Add(engineering);
            db.Faculties.Add(health);
            db.Faculties.Add(economics);
            db.Faculties.Add(other);

            // initialise  Departments here.
           d1.Faculty = engineering;
           d2.Faculty = engineering;
           d3.Faculty = engineering;
           d4.Faculty = engineering;
           
           nurse.Faculty = health;
           varnepleie.Faculty = health;
           db.Departments.AddRange(d1,d2,d3,d4,nurse,varnepleie);

           //adding departments to Engeenering Fuculty
           engineering.Departments.Add(d1);
           engineering.Departments.Add(d2);
           engineering.Departments.Add(d3);
           engineering.Departments.Add(d4);
           
           // adding health departments 
           health.Departments.Add(nurse);
           health.Departments.Add(varnepleie);
           
           db.SaveChanges();

        }
    }
}