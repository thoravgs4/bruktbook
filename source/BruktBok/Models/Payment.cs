﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BruktBok.Models
{
    public class Payment
    {
        public Payment(){}
        public string Id { get; set; }

        public DateTime Date { get; set; }
        
        [Required, DataType(DataType.EmailAddress)]
        public string BuyerEmail { get; set; }
        
        public int BookID { get; set; }
        
        public Book Book { get; set; }
    }
}