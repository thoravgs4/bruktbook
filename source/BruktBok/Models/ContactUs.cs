﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BruktBok.Models
{
    public class ContactUs
    {
        public string Email { get; set; }

        [Required, DataType(DataType.PhoneNumber)]

        [DisplayName("Melding til selger: ")] 
        public string Message { get; set; }  
    }
}