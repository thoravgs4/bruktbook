﻿#nullable enable
using System.Threading.Tasks;
using System;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;
namespace BruktBok.Models
{
    public interface  ISendPost
    {

        
        

        Task SendEmail(ContactSeller contactSeller, string ownerEmail, string ?url);

    }

    public class SendGridMialSerive : ISendPost 
    {
        private IConfiguration _configuration;
        
        public SendGridMialSerive(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        

        public  async Task SendEmail(ContactSeller contact, string ownerEmail, string? Urls)

        {
            var apiKey = _configuration["SenderGridIPKey"]; // Getting the API key from configuration file
            var client = new SendGridClient(apiKey);

            if (contact==null)
            {
                var froms = new EmailAddress(ownerEmail, "admin"); 
                var toVerify = new EmailAddress(ownerEmail, "admin");  // creating an EmailAddress object. Receiver email address.

                var msgs = MailHelper.CreateSingleEmail(froms,toVerify, Urls, "",Urls  );
                var responses = await client.SendEmailAsync(msgs);
                return;
                
            }
            
            // creating a SendGridClient object.
            var from = new EmailAddress(contact.Email,contact.Name); // creating an EmailAddress object. sender email address.
            var subject = "Mail from ";
            // this the email of the owner of the book 
            var to = new EmailAddress(ownerEmail, "admin");  // creating an EmailAddress object. Receiver email address.
            // var content= new Content(kontakt.PhoneNumber.ToString());
            
            var msg = MailHelper.CreateSingleEmail(from, to, subject,   contact.PhoneNumber.ToString(), contact.Message);
            var response = await client.SendEmailAsync(msg);
            
        }
        
    }
    
  
    
}

