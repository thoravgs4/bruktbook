﻿using System.ComponentModel.DataAnnotations;

namespace BruktBok.Models
{
    public class GostShopper
    {
        public GostShopper(string firstName,string lastName,
            string address,string email )
        {
            FirstName = firstName;
            LastName = lastName;
            Address = address;
            Email = email;
        }
        public int Id { get; set; }
        [Required]
        [Display(Name ="First Name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name ="Last Name")]
        public string LastName { get; set; }
        [Required]
        [Display(Name = "Address")]
        public string Address { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}