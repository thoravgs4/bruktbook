﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace BruktBok.Models
{
    /// <summary>
    /// custom App user. User owns account in this app.
    /// </summary>
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
            UserBooks = new List<Book>();
        }
        [Display(Name = "UserName")]
        public string NickName { get; set;}
        
        [Display(Name = "Profile Picture")]
        public string ImageUrl { get; set; }
        
        public List<Book> UserBooks { get; set; }
        
    }
}