﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace BruktBok.Models
{
    /// <summary>
    /// model for items to be sold on the website.
    /// </summary>
    
    public class Book
    {
      
        public Book(){}
        public Book(string title, string summary,decimal price, int departmentId )
        {
            Title = title;
            Summary = summary;
            Price = price; ;
            DepartmentId = departmentId;
            DepartmentBooks = new List<DepartmentBook>();
        }
        public int Id { get; set; }
        
        [Required]
        [Display(Name = "Tittel")]
        public string Title { get; set; }
        
        [Required]
        [Display(Name = "Sammendrag")]
        public string Summary { get; set; }

        [Required] [DataType(DataType.Date)]
        public string Posted { get; set; } = DateTime.Now.ToString("g")/*("dd.MM.yyyy")*/;
       
        [Required]
        [DataType(DataType.Currency)]
        [Display(Name = "Pris")]
        public decimal Price { get; set; }
        
        [Display(Name = "Beskrivelse")]
        public string Description { get; set; }
        // image variable
        [DataType(DataType.ImageUrl)]
        [Display(Name="Bilde")]
         public string ImageUrl { get; set; }
         
      
         public byte[] Content { get; set; }
        //owner
        public ApplicationUser ApplicationUser { get; set; }
       
        public string ApplicationUserId { get; set; }
        
        public  Department Department { get; set; }
       
        [Display(Name = "Studieprogram")]
        public  int DepartmentId { get; set; }
        // Departments this book belongs
        public List<DepartmentBook> DepartmentBooks { get; set; } = new List<DepartmentBook>();
    }
}
