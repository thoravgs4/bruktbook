using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BruktBok.Data;
using BruktBok.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;


namespace BruktBok
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
               // ApplicationDbInitializer.Initialize(scope.ServiceProvider);
                var context = services.GetRequiredService<ApplicationDbContext>();
                var environment = services.GetService<IWebHostEnvironment>();
                var um = services.GetRequiredService<UserManager<ApplicationUser>>();
                ApplicationDbInitializer.Initialize(context, environment.IsDevelopment(),um);
            }

            host.Run();
        
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}