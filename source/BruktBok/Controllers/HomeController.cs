﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BruktBok.Data;
using BruktBok.Models;
using BruktBok.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SQLitePCL;
using static BruktBok.Models.PostToOwner;
using Stripe;


namespace BruktBok.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _context;
        private readonly ISendPost _sendPost;

        public HomeController(ApplicationDbContext dbContext, ILogger<HomeController> logger, ISendPost sendPost)
        {
            _context = dbContext;
            _logger = logger;
            _sendPost = sendPost;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Index(int page)
        {
            int pageSize = 10;
            var totalBooks = _context.Books.Count();
            var totalPages = totalBooks / pageSize;
            var preveiousPage = page - 1;
            var nextPage = page + 1;

            ViewBag.PreviousPgae = preveiousPage;
            ViewBag.HasPreviousPage = preveiousPage >= 0;
            ViewBag.NextPage = nextPage;
            ViewBag.HasNextPage = nextPage < totalPages;
            var pv = new BookOverView();
            pv.Books = _context.Books.OrderBy(i => i.Id).Skip(pageSize * page).Take(pageSize).ToList();
            return View(pv);
        }

        [HttpGet]
        //When the user clicked Checkbox, this controller will invoke. 
        public IActionResult MultipleCheckboxes(string[] favorite)
        {
            //I could find better solution so I did this.
            string data = HttpContext.Request.Query["Data"].ToString();
            var books = new BookOverView();
            Console.WriteLine(data);

            var listOfBooks = from m in _context.Books
                select m;

            if (data == "Data")
            {
                books.Books = listOfBooks.Where(s => s.Department.Name.Contains(data)).ToList();
                return View(books);
            }

            if (data == "Elektro")
            {
                books.Books = listOfBooks.Where(s => s.Department.Name.Contains(data)).ToList();
                return View(books);
            }

            if (data == "Mekataronikk")
            {
                books.Books = listOfBooks.Where(s => s.Department.Name.Contains(data)).ToList();
                return View(books);
            }
            else
            {
                books.Books = listOfBooks.Where(s => s.Department.Name.Contains(data)).ToList();
                return View(books);
            }
        }

        //Action that charges for payment
        public async Task<IActionResult> Charge(int amount, string stripeToken, int bookID, string buyerEmail)
        {
            try
            {
                var book = await _context.Books.FindAsync(bookID);
                var sellerID = book.ApplicationUserId;
                var bookSeller = await _context.ApplicationUsers.FindAsync(sellerID);

                var customerOptions = new CustomerCreateOptions
                {
                    Description = "Payment for some books",
                    Source = stripeToken,
                    // Email = $"{user.Email}",
                    Email = bookSeller.Email,
                    Metadata = new Dictionary<string, string>
                    {
                        {"UniqueId", $"{Guid.NewGuid().ToString()}"}
                    }
                };

                var customerService = new CustomerService();
                var customer = customerService.Create(customerOptions);

                var options = new ChargeCreateOptions
                {
                    Amount = amount * 100,
                    Currency = "nok",
                    Description = "Add here your payment description",
                    Customer = customer.Id,
                    ReceiptEmail = buyerEmail
                };

                var service = new ChargeService();
                var charge = service.Create(options);
            }
            catch (StripeException)
            {
                return BadRequest(PaymentResultModel.EnumPR.Failed.ToString());
            }


            var payment = new Payment
            {
                Id = Guid.NewGuid().ToString(),
                Date = DateTime.Now,
                BookID = bookID,
                BuyerEmail = buyerEmail
            };

            _context.Payments.Add(payment);
            await _context.SaveChangesAsync();

            return Ok(new {result = PaymentResultModel.EnumPR.Success.ToString(), paymentId = payment.Id});
        }

        //Payment result action shows success or failed on payment

        public async Task<IActionResult> PaymentResult(PaymentResultModel.EnumPR result, string paymentId)
        {
            // generate random id.
            StringBuilder builder = new StringBuilder();
            Enumerable
                .Range(65, 26)
                .Select(e => ((char) e).ToString())
                .Concat(Enumerable.Range(97, 26).Select(e => ((char) e).ToString()))
                .Concat(Enumerable.Range(0, 10).Select(e => e.ToString()))
                .OrderBy(e => Guid.NewGuid())
                .Take(11)
                .ToList().ForEach(e => builder.Append(e));
            string id = builder.ToString();

            var bookId = _context.Payments.Find(paymentId).BookID;
            var emali = _context.Payments.Find(paymentId).BuyerEmail;
            var book = await _context.Books.FindAsync(bookId);

            var bookEmail = await _context.Books
                .Include(b => b.ApplicationUser)
                .FirstOrDefaultAsync(m => m.Id == bookId);
            var newBokId = bookId;
            var ownerEmail = book.ApplicationUser.Email;
            var contactSeller = new ContactSeller();
            // Den melding er til Owner til boka.   
            contactSeller.Email = "Thoravgs4@gmail.com";
            contactSeller.Message = "Hei, Din bok er solgt" + book.Title;


            ViewBag.Success = false;
            if (result == PaymentResultModel.EnumPR.Success)
            {
                ViewBag.Success = true;
                await Notification(contactSeller, bookEmail.ApplicationUser.Email);

                _context.Books.Remove(book);
                _context.SaveChanges();


                // lag en ny payment
                var paymentSucess = new PaymentSucess()
                {
                    OderNumber = id,
                    PaymentId = Guid.NewGuid().ToString(),
                    Date = DateTime.Now,
                    OwnerEmail =  ownerEmail,
                    BuyerEmail = emali
                };


                _context.PaymentSucesses.Add(paymentSucess);
                await _context.SaveChangesAsync();
                _context.SaveChanges();

                contactSeller.Email = "Thoravgs4@gmail.com";
                contactSeller.Message = "Takk for bestilling: Din order Id: " + paymentSucess.OderNumber;
                await VerificationOrder(contactSeller, paymentSucess.BuyerEmail);
                // await _sendPost.SendEmail(contactSeller, paymentSucess.BuyerEmail);
            }

            return View();
        }


        public async Task<IActionResult> VerificationOrder(ContactSeller contactSeller, string email)
        {
            await _sendPost.SendEmail(contactSeller, email, null);

            return Ok();
        }

        public async Task<IActionResult> Notification(ContactSeller contactSeller, string email)
        {
            await _sendPost.SendEmail(contactSeller, email, null);

            return Ok();
        }


        public async Task<IActionResult> IsAboutUs(ContactUs contactSeller)
        {
            string name = contactSeller.Email;
            Console.WriteLine(name);
            await SendToOwner(contactSeller);


            Console.Beep();
            return Redirect(nameof(Index));
        }


        [HttpGet]
        public async Task<IActionResult> PaymentForm(int? BookID)
        {
            if (BookID == null)
            {
                return NotFound();
            }


            var bookPayment = new BookPaymentModel();
            var book = await _context.Books.FindAsync(BookID);
            bookPayment.Book = book;
            return View(bookPayment);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}