﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BruktBok.Models
{
    /// <summary>
    /// Selger contact information. Used to send notification
    /// or interest in from shopper.
    /// </summary>
    public class ContactSeller
    {
        public ContactSeller(){}
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        [Required, DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required, DataType(DataType.PhoneNumber)]
        public int PhoneNumber { get; set; }

        [DisplayName("Melding til selger: ")] 
        public string Message { get; set; }   
        public Book Book { get; set; }
        public int BookId { get; set; }

    }
}