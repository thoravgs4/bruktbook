﻿$(document).ready(function(){
    const stripe = Stripe('pk_test_FiKqRSVTcg9d4vOy0v2FQevC00S3zniqrw');
    const elements = stripe.elements();

    const price = parseInt($('#price').val());
    const book = parseInt($('#BookID').val());
    let epost = $('#buyerEmail');
    
    const style = {
        base: {
            color: "#216599",
            fontWeight: 500,
            fontFamily: "Inter UI, Open Sans, Segoe UI, sans-serif",
            fontSize: "14px",
            fontSmoothing: "antialiased",
            border:"#216599",

            "::placeholder": {
                color: "#7a5849"
            }
        },
        invalid: {
            color: "#E25950"
        }

    };

    let card = elements.create("card", { style: style, hidePostalCode:true });
    card.mount("#card-element");

    card.addEventListener('change', ({error}) => {
        const displayError = document.getElementById('card-errors');
        if (error) {
            displayError.textContent = error.message;
        } else {
            displayError.textContent = '';
        }
    });

    const payBtn = document.getElementById('pay-with-stripe-btn');
    //payBtn.disabled = true;

    $(payBtn).unbind().bind('click', function(event){
        event.preventDefault();
        payBtn.disabled = true;

        epost = epost.val();

        stripe.createToken(card).then(function(result) {
            if (result.error) {
                // Inform the user if there was an error.
                const errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
                payBtn.disabled = false;
            } 
            else {
                // Send the token to your server.
                payBtn.disabled = true;
                stripeTokenHandler(result.token.id);
            }
        });
    });

// Submit the form with the token ID.
    function stripeTokenHandler(token) {
        payBtn.disabled = true;

        // const paymentValues = $("#payment-values");
        // const reason = $(paymentValues).attr("reason");
        // const amount = parseInt($(paymentValues).attr("amount"));
        
        $.ajax({
            type: "Post",
            url:"/Home/Charge/",
            data: {amount: price, stripeToken: token, bookID: book, buyerEmail: epost},
            success: function (response) {
                //window.location = "/payment/result/?result="+error.result+"&reason="+reason+"&message="+error.message;                
                window.location = "/Home/PaymentResult?result="+response.result+"&paymentId="+response.paymentId;
            },
            error: function (error) {

                //window.location = "/payment/result/?result="+error.result+"&reason="+reason+"&message="+error.message;
                window.location = "/Home/PaymentResult?result="+error.responseText;
            }
        })
    }

});