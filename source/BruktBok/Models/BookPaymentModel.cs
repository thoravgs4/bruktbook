﻿using Stripe;

namespace BruktBok.Models
{
    public class BookPaymentModel
    {
        public Book Book { get; set; }
        
        public Payment Payment { get; set; }
    }
}