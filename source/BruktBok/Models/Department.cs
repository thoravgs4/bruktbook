﻿using System.Collections.Generic;

namespace BruktBok.Models
{
    /// <summary>
    /// Category a book takes during registration 
    /// </summary>
    public class Department
    {
        public Department(){}
        public Department(string name)
        {
            Name = name;
            DepartmentBooks = new List<DepartmentBook>();
        }
        public int DepartmentId { get; set; }
        public string Name { get; set; }
        
        public List<DepartmentBook> DepartmentBooks { get; set; } = new List<DepartmentBook>();
        
        public int FacultyId { get; set; }
        public  Faculty Faculty { get; set; }
        
    }
}