using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BruktBok.Data;
using BruktBok.Models;
using BruktBok.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using static BruktBok.Models.ISendPost;


namespace BruktBok.Controllers
{
    public class BooksController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _um;
        private readonly IFileUpload _fileUpload;

        private ISendPost _sendPost;

        /// dependency for file uploader.
        private IWebHostEnvironment _env;

        public BooksController(ApplicationDbContext context, UserManager<ApplicationUser> um,
            IWebHostEnvironment env, IFileUpload fileUpload, ISendPost sendPost)
        {
            _context = context;
            _um = um;
            _env = env;
            _fileUpload = fileUpload;
            _sendPost = sendPost;
        }

        [HttpGet]
        public async Task<IActionResult> Index(string searchString)
        {
            var listOfBooks = from m in _context.Books
                select m;
            if (!String.IsNullOrEmpty(searchString))
            {
                listOfBooks = listOfBooks.Where(s => s.Title.Contains(searchString));
            }


            var books = new BookOverView
            {
                Books = await
                    listOfBooks.ToListAsync()
            };

            return View(books);
        }

        [Produces("application/json")]
        [HttpGet("getbooks")]
        public IActionResult GetBooks()

        { 
            var book = from m in _context.Books
                select m;

            try
            {
                string term = HttpContext.Request.Query["term"].ToString();
                var bookTitle = book.Where(p => p.Title.StartsWith(term)).Select(p => p.Title).ToList();
                return Ok(bookTitle);
            }
            catch
            {
                return BadRequest();
            }
        }


        
        [HttpGet]
        // GET: Books/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Books
                .Include(b => b.ApplicationUser)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (book == null)
            {
                return NotFound();
            }


            return View(book);
        }

        //GET;Books/Get Users Books
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> UserBook()
        {
            var user = _um.GetUserId(User);
            var userBooks = _context.Books.Where(book => book.ApplicationUserId == user)
                .OrderByDescending(bk => bk.Id);

            var books = new BookOverView {Books = await userBooks.ToListAsync()};

            return View(books);
        }

        // GET: Books/Create
        [HttpGet]
        [Authorize]
        public IActionResult Create()
        {
            var departments = new BookOverView();
            foreach (var department in _context.Departments)
            {
                departments.DepartmentList.Add(new SelectListItem
                {
                    Text = department.Name, Value = department.DepartmentId.ToString()
                });
            }

            ViewData["ApplicationUserId"] = new SelectList(_context.ApplicationUsers, "Id", "Id");
            //ViewData["DepartmentId"] = new SelectList(_context.Departments, "DepartmentId", "Name").ToList();
            return View(departments);
        }


        [HttpGet]
        public IActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Contact(ContactSeller contactSeller, int id)
        {
            var book = await _context.Books
                .Include(b => b.ApplicationUser)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (ModelState.IsValid)
            {

                await _sendPost.SendEmail(contactSeller,book.ApplicationUser.Email,"");


                Console.Beep();
            }

            return RedirectToAction(nameof(Contact));
        }

        // POST: Books/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
            [Bind("Id,Title,Summary,Description,Price,Content,DepartmentId,ApplicationUserId")]
            Book book, IFormFile file)
        {
            /*Image,*/
            if (ModelState.IsValid)
            {
                var userId = _um.GetUserId(User);
                _context.ApplicationUsers.Find(userId).UserBooks.Add(book);
                book.ApplicationUserId = userId;


                book.DepartmentBooks.Add(new DepartmentBook {BookId = book.Id, DepartmentId = book.DepartmentId});
                if (file != null)
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        await file.CopyToAsync(memoryStream);

                        // Upload the file if less than 2 MB
                        if (memoryStream.Length < 2097152)
                        {
                            book.Content = memoryStream.ToArray();
                            _context.Books.Add(book);
                            await _context.SaveChangesAsync();
                            return RedirectToAction("Index", "Home");
                        }
                        else
                        {
                            ModelState.AddModelError("File", "The file is too large.");
                        }
                    }
                }
            }

            /*if (ModelState.IsValid)
            {
                var userId = _um.GetUserId(User);
                _context.ApplicationUsers.Find(userId).UserBooks.Add(book);
                book.ApplicationUserId = userId;
                
                
                book.DepartmentBooks.Add(new DepartmentBook {BookId = book.Id, DepartmentId = book.DepartmentId});
                if (file != null)
                {
                    var filename = file.FileName;
                    var filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images", filename);
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }

                    book.ImageUrl = filename;
                }


                _context.Books.Add(book);

                await _context.SaveChangesAsync();
                return RedirectToAction("Index","Home");
            }*/

            ViewData["ApplicationUserId"] =
                new SelectList(_context.ApplicationUsers, "Id", "Id", book.ApplicationUserId);
            return View();
        }


        [Authorize]
        // GET: Books/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Books.FindAsync(id);

            if (book == null)
            {
                return NotFound();
            }

            if (!User.IsInRole("Admin") && (_um.GetUserId(User) != book.ApplicationUserId))
            {
                return RedirectToAction("Index");
            }

            ViewData["ApplicationUserId"] =
                new SelectList(_context.ApplicationUsers, "Id", "Id", book.ApplicationUserId);
            ViewData["DepartmentId"] = new SelectList(_context.Departments, "DepartmentId", "Name").ToList();
            return View(book);
        }

        // POST: Books/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,
            [Bind("Id,Title,Summary,Description,Price,Content,DepartmentId,ApplicationUserId")]
            Book book, IFormFile file)
        {
            if (id != book.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var user = _um.GetUserId(User);
                    book.ApplicationUserId = user;
                   // _fileUpload.uploadImage(file);
                   
                    if (file != null)
                    {
                       // book.ImageUrl = file.FileName;
                        
                        using (var memoryStream = new MemoryStream())
                        {
                            await file.CopyToAsync(memoryStream);

                            // Upload the file if less than 2 MB
                            if (memoryStream.Length < 2097152)
                            {
                                book.Content = memoryStream.ToArray();
                                
                            }else
                            {
                                ModelState.AddModelError("File", "The file is too large.");
                            }
                        }
                    }

                    _context.Update(book);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookExists(book.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToAction(nameof(Index));
            }

            ViewData["ApplicationUserId"] =
                new SelectList(_context.ApplicationUsers, "Id", "Id", book.ApplicationUserId);
            return View(book);
        }

        [Authorize]
        // GET: Books/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Books
                .Include(b => b.ApplicationUser)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (book == null)
            {
                return NotFound();
            }

            return View(book);
        }

        // POST: Books/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var book = await _context.Books.FindAsync(id);
            _context.Books.Remove(book);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BookExists(int id)
        {
            return _context.Books.Any(e => e.Id == id);
        }
    }
}