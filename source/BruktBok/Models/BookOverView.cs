﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace BruktBok.Models
{
    public class BookOverView
    {
        public BookOverView()
        {
            FacultyList = new List<SelectListItem>();
            Books = new List<Book>();
            DepartmentList = new List<SelectListItem>();
        }
        public Book Book { get; set; }
        public List<Book>Books { get; set; }
        public Faculty Faculty { get; set; }

        public List<SelectListItem> FacultyList { get; set; }
      
        public  List<SelectListItem> DepartmentList{ get; set; }
        public List<Faculty>Faculties { get; set; }
        public Department Department { get; set; }
        public List<Department>Departments { get; set; }

    }
}
